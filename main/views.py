from django.shortcuts import render
from django.http import JsonResponse
import requests

def home(request):
    return render(request, 'main/home.html')

def cari(request):
    url = "https://www.googleapis.com/books/v1/volumes"
    response = requests.get(url, params={
                                         'q' : request.GET['q'], 
                                         'startIndex' : request.GET['p']
                                        })
    #print(request.GET['p'])
    return JsonResponse(response.json())