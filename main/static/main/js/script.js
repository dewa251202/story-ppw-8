var pageNow = 0;
var pageMax = 0;

$(function(){
    $("#book-search").on("input", cariAwal);
    $(".nav-btn").on("click", function(){
       if($(this).attr("id") == "kiri"){
           if(pageNow > 0){
               pageNow--;
               cari();
           }
       }
       else{
           if(pageNow < pageMax - 1){
               pageNow++;
               cari();
           }
       }
    });
});

function setOpacity(){
    $("#kiri").css("opacity", (pageNow == 0 ? "0.5" : "1"));
    $("#kanan").css("opacity", (pageNow == pageMax ? "0.5" : "1"));
    console.log("Maximum page: " + pageMax);
}

function cariAwal(){
    pageNow = 0;
    pageMax = 0;
    cari();
}

function cari(){
    var judul_buku = $("#book-search").val();
    $.ajax({
        url: "/cari",
        data: {
            q: judul_buku,
            p: pageNow
        },
        success: function(data){
            pageMax = Math.ceil(data.totalItems/10) - 1;
            $("#page-num").text(pageNow + 1);
            setOpacity();
            
            var books = data.items;
            var hasil = $("#hasil-pencarian");
            hasil.empty();
            if(books){
                $("#hasil").css("display", "flex");
                //console.log(books);
                for(var i = 0; i < books.length; i++){
                    //console.log(books[i].volumeInfo.title);
                    var bookInfo = books[i].volumeInfo;
                    var item = $("<div></div>").attr("class", "item-pencarian");
                    
                    var bookTitle = $("<div></div>").text(bookInfo.title).css("padding", "1em");
                    var bookImage = $("<img></img>").attr({
                        class: "hasil-gambar",
                        src: bookInfo.imageLinks.thumbnail
                    });
                    
                    item.append(bookTitle);
                    item.append(bookImage);
                    hasil.append(item);
                }
            }
            else{
                $("#hasil").css("display", "none");
            }
        }
    });
}